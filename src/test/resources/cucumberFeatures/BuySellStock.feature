Feature: Buy or Sell Stock
  As a stock holder
  I want to sell or buy stock as per the new stock price
  So that I can manage my portfolio.

  Scenario: User should buy five stocks
    Given that user is on stocks page
    When user clicks on check price button one
    Then stock quantity should increase to 10 from 5
    
  Scenario: User should sell five stocks
    Given that user is on stocks page
    When user clicks on check price button two
    Then stock quantity should decrease to 20 from 25
    

