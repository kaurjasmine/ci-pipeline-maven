package stepDefinitions;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BuySellStockSteps {
	
	WebDriver driver = null;
	
	@Before 
	public void setup() throws MalformedURLException {

        String location = System.getProperty("webdriver.chrome.driver");
        System.setProperty("webdriver.chrome.driver", location);
        ChromeOptions chromeOptions= new ChromeOptions();
        chromeOptions.addArguments("--window-size=1920,1080");
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@Given("^that user is on stocks page$")
	public void that_user_is_on_stocks_page() throws InterruptedException {
		driver.get("http://localhost:8081/ci-pipeline-maven/index.jsf");
		Thread.sleep(1000);
	}

	@When("^user clicks on check price button one$")
	public void user_clicks_on_check_price_button_one() {
		driver.findElement(By.id("check_price_btn1")).click();
	}
	
	@When("^user clicks on check price button two$")
	public void user_clicks_on_check_price_button_two() {
		driver.findElement(By.id("check_price_btn2")).click();
	}
	
	@Then("^stock quantity should increase to (\\d+) from (\\d+)$")
	public void stock_quantity_should_increase_to_from(int expectedQty, int arg2) {
	    String getQty = driver.findElement(By.id("qty1")).getText();
	    assertEquals(String.valueOf(expectedQty), getQty);
	}

	@Then("^stock quantity should decrease to (\\d+) from (\\d+)$")
	public void stock_quantity_should_decrease_to_from(int expectedQty, int arg2) {
		String getQty = driver.findElement(By.id("qty2")).getText();
		assertEquals(String.valueOf(expectedQty), getQty);
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}

}
